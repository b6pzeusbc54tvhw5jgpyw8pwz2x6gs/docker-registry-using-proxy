docker stop registry
docker rm registry
docker run -d --restart=always \
  --name registry \
  --env VIRTUAL_HOST=registry.devp.me \
  --env VIRTUAL_PORT=5000 \
  --expose 5000 \
  -e "REGISTRY_STORAGE=s3" \
  -e "REGISTRY_STORAGE_S3_REGION=ap-northeast-2" \
  -e "REGISTRY_STORAGE_S3_BUCKET=wsc-docker-registry" \
  -e "REGISTRY_STORAGE_S3_ACCESSKEY=xxxxxxxxxxxxxxxxxxxx" \
  -e "REGISTRY_STORAGE_S3_SECRETKEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" \
  -e "HTTP_PROXY=http://proxydi:proxypw@123.123.123.123:8080" \
  -v $PWD/conf:/var/lib/registry registry:2 /var/lib/registry/config.yml \
  registry:2
